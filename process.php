<?php
    require 'db_connection.php';
   
    if (($file = fopen("messages.csv", "r")) !== FALSE) {
        $headerLine = true;
        while (($data = fgetcsv($file, 10000, ",")) !== FALSE) {
            if($headerLine) {
                 $headerLine = false; 
            }
            else{
           
                try {

                    
                    $stmt = $conn->prepare("INSERT INTO call_logs(other_id, _number, duration, _length, direction) VALUES (:other_id, :_number, :duration, :_length, :direction)");
                    
                    $stmt->bindParam(':other_id', $other_id);
                    $stmt->bindParam(':_number', $number);
                    $stmt->bindParam(':duration', $duration);
                    $stmt->bindParam(':_length', $length);
                    $stmt->bindParam(':direction', $direction);
                
                    $number    = $data[0];
                    $other_id  = $data[1];
                    $direction = $data[2];
                    $length    = $data[3];
                    $duration  = strtotime($data[4]);
                   
                    if ( $stmt->execute()){
                        echo 'data inserted';
                    }
                }
                    catch(PDOException $e)
            {
                    echo "Error: " . $e->getMessage();
            }
        
        }
    }
        fclose($file);
    }

  
    
?>