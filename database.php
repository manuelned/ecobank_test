<?php

    require 'db_connection.php';
    
    $sql = "CREATE TABLE IF NOT EXISTS `call_logs` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `other_id` varchar(20) DEFAULT NULL,
        `_number` varchar(20) NOT NULL,
        `duration` varchar(20),
        `_length` varchar(20),
        `direction` varchar (20),
        `created_at` DATETIME NOT NULL DEFAULT NOW(),
        `updated_at` DATETIME DEFAULT NULL,
        PRIMARY KEY (id))";
    
    $sql2 = "CREATE TABLE IF NOT EXISTS `roles` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `user_id` varchar(20) NOT NULL,
        `name`  varchar(20) NOT NULL,
        `created_at` DATETIME NOT NULL DEFAULT NOW(),
        `updated_at` DATETIME DEFAULT NULL,
        PRIMARY KEY (id))";
     
     $stm = $conn->prepare($sql2);
           
     $exec = $stm->execute();

     $sql3 = "CREATE TABLE IF NOT EXISTS `permissions` (
        `id` int(20) NOT NULL AUTO_INCREMENT,
        `user_id` varchar(20) NOT NULL,
        `name`  varchar(20) NOT NULL,
        `created_at` DATETIME NOT NULL DEFAULT NOW(),
        `updated_at` DATETIME DEFAULT NULL,
        PRIMARY KEY (id))";

            $stm = $conn->prepare($sql3);
           
             $exec = $stm->execute();
       

    if ($exec) {
        echo  "table successfully created";
    }

    else{
        echo "error creating table";
    }

?>